package org.torvis.component.security.password;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;

/**
 * A password field with a password generator.
 * 
 * @author martin.letendre@release5.com
 *
 */
public class PasswordAdvanceField extends GridLayout {

    /** IDE. */
    private static final long serialVersionUID = 4074501150969618307L;

    /**
     * Used only if the password is invisible.
     */
    private PasswordField passwordField = new PasswordField();

    /**
     * Used only if the password is visible.
     */
    private TextField visiblePasswordField = new TextField();

    /**
     * Button that can have three modes.
     */
    private PasswordGeneratorButton passwordGeneratorButton = new PasswordGeneratorButton(this);

    /**
     * A password field with a password generator.
     * 
     * @param caption
     *            The caption for the fields.
     */
    public PasswordAdvanceField(String caption) {

	super(2, 1);

	setStyleAndLayout();

	initializePasswordFields(caption);

	setPassword("");

	hidePassword();

	addPasswordGeneratorButton();

    }

    /**
     * width of the inner password field.
     * 
     * @param width With.
     * @param unit Example: Unit.PIXELS
     */
    public void setTextFieldWidth(int width, Unit unit) {

	passwordField.setWidth(width, unit);

	visiblePasswordField.setWidth(width, unit);
    }

    /**
     * The unencrypted password.
     * 
     * @return The unencrypted password.
     */
    public String getPassword() {

	return passwordField.getValue().trim();
    }

    /**
     * Show the password.
     */
    void showPassword() {

	removeComponent(0, 0);

	addComponent(visiblePasswordField, 0, 0);

    }

    /**
     * Show password bullets.
     */
    void hidePassword() {

	removeComponent(0, 0);

	addComponent(passwordField, 0, 0);
    }

    /**
     * Change the password value.
     * 
     * @param generatedPassword
     *            The new value.
     */
    void setPassword(String generatedPassword) {

	generatedPassword = generatedPassword.trim();

	passwordField.setValue(generatedPassword);

	visiblePasswordField.setValue(generatedPassword);

	passwordGeneratorButton.setPassword(generatedPassword);

    }

    /**
     * Initialize the password fields.
     * 
     * @param caption
     *            The component's caption <code>String</code>. Caption is the
     *            visible name of the component.
     */
    private void initializePasswordFields(String caption) {

	passwordField.setCaption(caption);

	passwordField.addTextChangeListener(event -> {

	    String value = (String) event.getText();

	    setPassword(value);

	});

	visiblePasswordField.setCaption(caption);

	visiblePasswordField.addTextChangeListener(event -> {

	    String value = (String) event.getText();

	    setPassword(value);

	});

    }

    /**
     * The style and layout.
     */
    private void setStyleAndLayout() {

	setSpacing(true);

    }

    /**
     * Button that show the password generator modal window.
     */
    private void addPasswordGeneratorButton() {

	addComponent(passwordGeneratorButton, 1, 0);

	setComponentAlignment(passwordGeneratorButton, Alignment.BOTTOM_LEFT);

    }

}
