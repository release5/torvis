package org.torvis.component.security.password;

/**
 * The button has 3 modes.
 * <ol>
 * <li><strong>GENERATE</strong>.Show the password generator. Only if the field
 * is empty.</li>
 * <li><strong>SHOW_PASSWORD</strong>.Show the password. Only if the field is
 * has a password that is hidden.</li>
 * <li><strong>HIDE_PASSWORD</strong>.Hide the password.Only if the field is has
 * a password that is present.</li>
 * </ol>
 * 
 * @author martin.letendre@release5.com
 *
 */
enum PasswordGeneratorButtonMode {

	/** Show the password generator. */
    GENERATE,

	/** Show the password. */
    SHOW_PASSWORD,

	/** Hide the password. */
    HIDE_PASSWORD;

}
