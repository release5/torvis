package org.torvis.component.security.password;

import org.torvis.component.util.InternatializationUtil;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Window;

/**
 * Contains the PasswordGeneratorPanel.
 * 
 * @see org.torvis.component.security.password.PasswordGeneratorContent
 * 
 * @author martin.letendre@release5.com
 *
 */
class PasswordGeneratorWindow extends Window {

    /** IDE. */
    private static final long serialVersionUID = -7137636077464509570L;

    private InternatializationUtil i18nUtil = new InternatializationUtil();

    /**
     * Contains the PasswordGeneratorPanel.
     * 
     * @param passwordAdvanceField
     *            A password field with a password generator.
     */
    PasswordGeneratorWindow(PasswordAdvanceField passwordAdvanceField) {

	PasswordGeneratorContent content = new PasswordGeneratorContent(this, passwordAdvanceField);

	center();

	setContent(content);

	setModal(true);

	final String title = FontAwesome.LOCK.getHtml() + " " + i18nUtil.getProperty("password.generator.window.title");

	setCaption(title);

	setCaptionAsHtml(true);

	setWidth(450, Unit.PIXELS);

	setHeight(400, Unit.PIXELS);

	setResizable(false);

    }

}
