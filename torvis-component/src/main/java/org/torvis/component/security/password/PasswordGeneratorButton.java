package org.torvis.component.security.password;

import org.apache.commons.lang.StringUtils;
import org.torvis.component.util.InternatializationUtil;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.BaseTheme;

/**
 * Button that can have three different behavior.
 * <ol>
 * <li>Show the password generator. Only if the field is empty.</li>
 * <li>Show the password. Only if the field is has a password that is hidden.
 * </li>
 * <li>Hide the password.Only if the field is has a password that is present.
 * </li>
 * </ol>
 * 
 * @author martin.letendre@release5.com
 *
 */
class PasswordGeneratorButton extends Button {

    /**
     * The button mode.
     * 
     * @see org.torvis.component.security.password.PasswordGeneratorButtonMode
     */
    private PasswordGeneratorButtonMode mode = PasswordGeneratorButtonMode.GENERATE;

    /**
     * Internatialization (i18n) util.
     */
    private InternatializationUtil i18nUtil = new InternatializationUtil();

    /** IDE. */
    private static final long serialVersionUID = 2114402017662595732L;

    /**
     * Button that can have three different behavior.
     * 
     * @param passwordAdvanceField
     *            A password field with a password generator.
     */
    PasswordGeneratorButton(PasswordAdvanceField passwordAdvanceField) {

	String description = i18nUtil.getProperty("password.button.description");

	setIcon(FontAwesome.REPEAT);

	setStyleName(BaseTheme.BUTTON_LINK);

	setDescription(description);

	addClickListener(event -> {

	    switch (mode) {

	    case SHOW_PASSWORD:

		showPassword(passwordAdvanceField);

		break;

	    case HIDE_PASSWORD:

		hidePassword(passwordAdvanceField);

		break;

	    default:

		showPasswordGenerator(passwordAdvanceField);

		break;
	    }

	});

    }

    /**
     * Button Mode.
     * 
     * @return GENERATE or SHOW_PASSWORD or HIDE_PASSWORD;
     * 
     * @see org.torvis.component.security.password.PasswordGeneratorButtonMode
     */
    PasswordGeneratorButtonMode getMode() {

	return mode;

    }

    /**
     * Must set the password to evaluate the current mode.
     * 
     * @param password
     *            An unencrypted password.
     */
    void setPassword(String password) {

	if (StringUtils.isBlank(password)) {

	    mode = PasswordGeneratorButtonMode.GENERATE;

	    setIcon(FontAwesome.REPEAT);

	} else if (mode == PasswordGeneratorButtonMode.GENERATE) {

	    mode = PasswordGeneratorButtonMode.SHOW_PASSWORD;

	    setIcon(FontAwesome.EYE);

	}

    }

    /**
     * Set the button to GENERATE mode. If you click the button it shows the
     * password generator.
     * 
     * @param passwordAdvanceField
     *            A password field with a password generator.
     */
    private void showPasswordGenerator(PasswordAdvanceField passwordAdvanceField) {

	mode = PasswordGeneratorButtonMode.GENERATE;

	setIcon(FontAwesome.REPEAT);

	PasswordGeneratorWindow passwordGeneratorWindow = new PasswordGeneratorWindow(passwordAdvanceField);

	UI.getCurrent().addWindow(passwordGeneratorWindow);
    }

    /**
     * Set the button to SHOW_PASSWORD mode. If you click the button it replace
     * the password with <strong>password bullets</strong>.
     * 
     * @param passwordAdvanceField
     *            A password field with a password generator.
     */
    private void hidePassword(PasswordAdvanceField passwordAdvanceField) {

	passwordAdvanceField.hidePassword();

	mode = PasswordGeneratorButtonMode.SHOW_PASSWORD;

	setIcon(FontAwesome.EYE);

    }

    /**
     * Set the button to SHOW_PASSWORD mode. If you click the button it replace
     * <strong>"password bullets</strong> with the current password.
     * 
     * @param passwordAdvanceField
     *            A password field with a password generator.
     */
    private void showPassword(PasswordAdvanceField passwordAdvanceField) {

	passwordAdvanceField.showPassword();

	mode = PasswordGeneratorButtonMode.HIDE_PASSWORD;

	setIcon(FontAwesome.EYE_SLASH);

    }

}
