package org.torvis.component.security.password;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.WordUtils;
import org.torvis.component.util.InternatializationUtil;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Slider;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.BaseTheme;

/**
 * A password generator UI component. You can select these criteria.
 * 
 * <ol>
 * <li>The length of the password [8-128]</li>
 * <li>Include alphabetic characters</li>
 * <li>Include numeric characters</li>
 * <li>Include uppercase characters</li>
 * </ol>
 * 
 * @author martin.letendre@release5.com
 *
 */
class PasswordGeneratorContent extends VerticalLayout {

    /** IDE. */
    private static final long serialVersionUID = 5144557375417772969L;

    /**
     * Internatialization util.
     */
    private InternatializationUtil i18nUtil = new InternatializationUtil();

    /**
     * Contains the PasswordGeneratorPanel.
     */
    private PasswordGeneratorWindow window;

    /**
     * A password field with a password generator.
     */
    private PasswordAdvanceField passwordAdvanceField;

    /**
     * Display the password.
     */
    private Label passwordLabel = new Label();

    /**
     * Minimum password length.
     */
    private static final int MIN_PASSWORD_LENGTH = 8;

    /**
     * To select a password length different than 8.
     */
    private Slider passwordLengthSlider = new Slider(MIN_PASSWORD_LENGTH, 128);

    /**
     * Use alphabetic characters (in password) checkbox.
     */
    private CheckBox alphabeticCheckBox = new CheckBox("", true);

    /**
     * Use numeric characters (in password) checkbox.
     */
    private CheckBox numericCheckBox = new CheckBox("", true);

    /**
     * Use upercase characters (in password) checkbox.
     */
    private CheckBox uppercaseCheckBox = new CheckBox("", true);

    /**
     * The temporary password.
     */
    private String tempGeneratedPassword;

    /**
     * A password generator UI component.
     * 
     * @param window
     *            Contains the PasswordGeneratorPanel.
     * @param passwordAdvanceField
     *            A password field with a password generator.
     */
    PasswordGeneratorContent(PasswordGeneratorWindow window, PasswordAdvanceField passwordAdvanceField) {

	this.window = window;

	this.passwordAdvanceField = passwordAdvanceField;

	setStyleAndLayout();

	addPasswordLayout();

	addPasswordLengthSlider();

	addNumericCharactersCheckBox();

	addAlphabeticCharactersCheckBox();

	addUpercaseCharactersCheckBox();

	addButtons();

    }

    /**
     * Generated password may include numeric characters.
     */
    private void addAlphabeticCharactersCheckBox() {

	addComponent(alphabeticCheckBox);

	String caption = i18nUtil.getProperty("password.checkbox.alphabetic");

	alphabeticCheckBox.setCaption(caption);

	alphabeticCheckBox.addValueChangeListener(event -> {

	    Boolean enabled = (Boolean) event.getProperty().getValue();

	    uppercaseCheckBox.setEnabled(enabled);

	    if (!enabled) {

		uppercaseCheckBox.setValue(false);
	    }

	    refreshPassword();

	});
    }

    /**
     * Generated password may include alphabetic characters.
     */
    private void addNumericCharactersCheckBox() {

	addComponent(numericCheckBox);

	String caption = i18nUtil.getProperty("password.checkbox.numeric");

	numericCheckBox.setCaption(caption);

	numericCheckBox.addValueChangeListener(event -> {

	    refreshPassword();
	});
    }

    /**
     * Generated password may include uppercase characters.
     */
    private void addUpercaseCharactersCheckBox() {

	addComponent(uppercaseCheckBox);

	String caption = i18nUtil.getProperty("password.checkbox.uppercase");

	uppercaseCheckBox.setCaption(caption);

	uppercaseCheckBox.addValueChangeListener(event -> {

	    refreshPassword();
	});
    }

    /**
     * Add password box.
     */
    private void addPasswordLayout() {

	HorizontalLayout passwordLayout = new HorizontalLayout();

	passwordLayout.setHeight(100, Unit.PIXELS);

	addComponent(passwordLayout);

	passwordLayout.addStyleName("password-layout");

	passwordLayout.setWidth(100, Unit.PERCENTAGE);

	refreshPassword();

	addPassworValue(passwordLayout);

	addPasswordReload(passwordLayout);

    }

    /**
     * Add password value.
     * 
     * @param passwordLayout
     *            The surrounding box.
     */
    private void addPassworValue(HorizontalLayout passwordLayout) {

	passwordLabel.setCaptionAsHtml(true);

	passwordLabel.setSizeUndefined();

	passwordLayout.addComponent(passwordLabel);

	passwordLayout.setComponentAlignment(passwordLabel, Alignment.MIDDLE_CENTER);

	passwordLayout.setExpandRatio(passwordLabel, 1);
    }

    /**
     * Add password reload button.
     * 
     * @param passwordLayout
     *            The surrounding box.
     */
    private void addPasswordReload(HorizontalLayout passwordLayout) {

	Button button = new Button();

	String description = i18nUtil.getProperty("password.button.reload.description");

	button.setDescription(description);

	button.setIcon(FontAwesome.REPEAT);

	passwordLayout.addComponent(button);

	passwordLayout.setExpandRatio(button, 0);

	passwordLayout.setComponentAlignment(button, Alignment.MIDDLE_RIGHT);

	button.addStyleName(BaseTheme.BUTTON_LINK);

	button.addClickListener(event -> {

	    refreshPassword();

	});

    }

    /**
     * Refresh the password when the user hit reload or change one of these 4
     * paramerters.
     * 
     * <ol>
     * <li>The length of the password [8-128]</li>
     * <li>Include (or not) alphabetic characters</li>
     * <li>Include (or not) numeric characters</li>
     * <li>Include (or not) uppercase characters</li>
     * </ol>
     */

    private void refreshPassword() {

	int passwordLength = passwordLengthSlider.getValue().intValue();

	String generatedPassword = RandomStringUtils.random(passwordLength, alphabeticCheckBox.getValue(),
		numericCheckBox.getValue());

	Boolean isUppercase = uppercaseCheckBox.getValue();

	if (!isUppercase) {

	    generatedPassword = generatedPassword.toLowerCase();

	}

	tempGeneratedPassword = generatedPassword;

	generatedPassword = WordUtils.wrap(generatedPassword, 32, "<br />", true);

	passwordLabel.setCaption(generatedPassword);

    }

    /**
     * Add password length slider.
     */
    private void addPasswordLengthSlider() {

	changePasswordLength();

	passwordLengthSlider.setCaptionAsHtml(true);

	passwordLengthSlider.setWidth(100, Unit.PERCENTAGE);

	addComponent(passwordLengthSlider);

	passwordLengthSlider.addValueChangeListener(event -> {

	    changePasswordLength();

	});

    }

    /**
     * Reaction to changing the password length.
     */
    private void changePasswordLength() {

	final String passwordLengthCaption = i18nUtil.getProperty("password.generator.password.length");

	int passwordLength = passwordLengthSlider.getValue().intValue();

	passwordLengthSlider.setCaption(passwordLengthCaption + ": <strong>" + passwordLength + "</strong>");

	refreshPassword();

    }

    /**
     * Add save and cancel buttons.
     */
    private void addButtons() {

	HorizontalLayout buttonLayout = new HorizontalLayout();

	buttonLayout.setWidth(100, Unit.PERCENTAGE);

	addComponent(buttonLayout);

	addCancelButton(buttonLayout);

	addSaveButton(buttonLayout);

    }

    /**
     * Style and layout of the component.
     */
    private void setStyleAndLayout() {

	setMargin(true);

	setSpacing(true);

	setWidth(100, Unit.PERCENTAGE);
    }

    /**
     * Add save button.
     * 
     * @param buttonLayout
     *            The layout that contains the button (for alignement).
     */
    private void addSaveButton(HorizontalLayout buttonLayout) {

	final String saveCaption = i18nUtil.getProperty("button.save");

	Button saveButton = new Button(saveCaption);

	saveButton.addClickListener(event -> {

	    saveGeneratedPassword();

	});

	saveButton.setIcon(FontAwesome.SAVE);

	buttonLayout.addComponent(saveButton);

	buttonLayout.setComponentAlignment(saveButton, Alignment.TOP_RIGHT);

	buttonLayout.setExpandRatio(saveButton, 0);
    }

    /**
     * Reaction to clicking the save button.
     */
    private void saveGeneratedPassword() {

	passwordAdvanceField.setPassword(tempGeneratedPassword);

	window.close();
    }

    /**
     * Reaction to clicking the cancel button.
     * 
     * @param buttonLayout
     *            The layout that contains the button (for alignement).
     */
    private void addCancelButton(HorizontalLayout buttonLayout) {

	final String cancelCaption = i18nUtil.getProperty("button.cancel");

	Button cancelButton = new Button(cancelCaption);

	cancelButton.addClickListener(event -> {

	    window.close();

	});

	cancelButton.setIcon(FontAwesome.TIMES_CIRCLE_O);

	buttonLayout.addComponent(cancelButton);

	buttonLayout.setComponentAlignment(cancelButton, Alignment.TOP_RIGHT);

	buttonLayout.setExpandRatio(cancelButton, 1);

	buttonLayout.setSpacing(true);
    }

}
