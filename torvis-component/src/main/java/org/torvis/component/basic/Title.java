package org.torvis.component.basic;

import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * Defines a title for a section of the document.
 * 
 * @author martin.letendre@release5.com
 *
 */
public class Title extends VerticalLayout {

    /** IDE. */
    private static final long serialVersionUID = -1589279803375568891L;

    /**
     * Defines a title for a section of the document.
     * 
     * @param caption
     *            The title.
     * @param icon
     *            A FontAwesome icon.
     */
    public Title(String caption, FontAwesome icon) {

	this.addComponent(new Separator());

	addCaption(icon.getHtml() + " " + caption);

	setStyleAndLayout();

    }

    /**
     * Add the caption section.
     * 
     * @param caption
     *            The component's caption <code>String</code>. Caption is the
     *            visible name of the component.
     */
    private void addCaption(String caption) {

	Label label = new Label(caption);

	label.setContentMode(ContentMode.HTML);

	addComponent(label);

	label.addStyleName("title");
    }

    /**
     * The style and layout.
     */
    private void setStyleAndLayout() {

	setSizeFull();

	setSpacing(false);

	setMargin(false);

    }

}
