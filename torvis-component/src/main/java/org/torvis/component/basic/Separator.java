package org.torvis.component.basic;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;

/**
 * Define a thematic change in the content.
 * 
 * @author martin.letendre@release5.com
 *
 */
public class Separator extends Label {

    /** IDE. */
    private static final long serialVersionUID = 2750147142801254792L;

    /**
     * HTML code for the separator.
     */
    private static final String HTML = "<div class='separator' ></div><div class='separator-motif' ></div>";

    /**
     * Define a thematic change in the content.
     */
    public Separator() {

	super(HTML);

	setContentMode(ContentMode.HTML);

	setHeight(16, Unit.PIXELS);

    }

}
