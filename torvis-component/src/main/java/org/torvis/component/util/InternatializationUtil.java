package org.torvis.component.util;

import java.io.File;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.torvis.component.security.password.PasswordAdvanceField;

/**
 * Internatialization (i18n) util.
 * 
 * @author martin.letendre@release5.com
 *
 */
public class InternatializationUtil {

    /**
     * This is the "classic" Properties loader which loads the values from a
     * single or multiple files (which can be chained with "include =". All
     * given path references are either absolute or relative to the file name
     * supplied in the constructor.
     */
    private PropertiesConfiguration config = new PropertiesConfiguration();

    /**
     * If you spell Chuck Norris in Scrabble, you win, forever.
     */
    private static final Logger logger = Logger.getLogger(PasswordAdvanceField.class);

    /**
     * Internatialization (i18n) util.
     */
    public InternatializationUtil() {

	try {

	    File file = new File("components-labels.properties");

	    config.setFile(file);

	    config.load();

	} catch (Exception exception) {

	    logger.error(exception);
	}
    }

    /**
     * Value of this property with the specified key value.
     * 
     * @param key
     *            The key.
     * @return Value of this property.
     */
    public String getProperty(String key) {

	return (String) config.getProperty(key);
    }

}
