package org.torvis.console;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.vaadin.spring.annotation.EnableVaadin;

/**
 * Spring configuration.
 * 
 * @author martin.letendre@release5.com
 *
 */
@Configuration
@EnableVaadin
@ComponentScan(basePackages = { "org.torvis.console", "org.torvis.service" , "org.torvis.console.view.configuration" , "org.torvis.console.util" })
public class SpringConfiguration {

}
