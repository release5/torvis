package org.torvis.console.view.configuration;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;

/**
 * The <strong>Torvis Console</strong> configuration View. This View is
 * displayed if the <strong>Torvis Console</strong> has not found the
 * consiguration file uner "$HOME/.torvis/console-configuration.json".This page
 * is displayed instead of the login page.
 * 
 * @author martin.letendre@release5.com
 *
 */
@SpringView(name = ConfigurationView.VIEW_NAME)
class ConfigurationView extends VerticalLayout implements View {

    /**
     * It is the default view.
     */
    public static final String VIEW_NAME = "";

    /** IDE. */
    private static final long serialVersionUID = -4445667367123687193L;


    @Autowired
    private WelcomeMessageSection welcomeMessageSection;

    /**
     * Minimal General <strong>Torvis</strong> preferences.
     */
    @Autowired
    private GeneralParameters generalParameters;

    /**
     * Network connection parameters.
     */
    @Autowired
    private NetworkParameters networkParameters;

    /**
     * Initialize the View.
     */
    @PostConstruct
    void init() {

	addComponent(welcomeMessageSection);

	addComponent(generalParameters);

	addComponent(networkParameters);

    }

    /**
     * This method is always called before the view is shown on screen.
     */
    @Override
    public void enter(ViewChangeEvent event) {

    }
}
