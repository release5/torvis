package org.torvis.console.view.configuration;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.torvis.component.basic.Title;
import org.torvis.component.security.password.PasswordAdvanceField;
import org.torvis.console.util.PresentationUtil;
import org.torvis.service.services.InternatializationService;

import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * Minimal General <strong>Torvis</strong> preferences.
 * 
 * @author martin.letendre@release5.com
 *
 */
@Component
class GeneralParameters extends VerticalLayout {

    /** IDE. */
    private static final long serialVersionUID = 7563528597208010494L;

    /**
     * Provide i18n services.
     */
    @Autowired
    private InternatializationService i18nService;

    @PostConstruct
    void init() {

	setStyleAndLayout();

	addConsoleParamTitle();

	addAdministratorUsername();

	addAdministratorPassword();

    }

    /**
     * The administrator username. (The <strong>root</strong> username.
     */
    private void addAdministratorUsername() {

	String caption = i18nService.getProperty("configuration.administrator.username");

	TextField field = new TextField(caption);

	String defaultValue = i18nService.getProperty("configuration.administrator.username.default");

	field.setValue(defaultValue);

	field.setWidth(PresentationUtil.DEFAULT_WIDTH, Unit.PIXELS);

	addComponent(field);

    }

    /**
     * The administrator password. (The <strong>root</strong> password.
     */
    private void addAdministratorPassword() {

	String caption = i18nService.getProperty("configuration.administrator.password");

	PasswordAdvanceField field = new PasswordAdvanceField(caption);

	field.setTextFieldWidth(PresentationUtil.DEFAULT_WIDTH, Unit.PIXELS);

	addComponent(field);
    }

    /**
     * Title for the general parameters.
     */
    private void addConsoleParamTitle() {

	String caption = i18nService.getProperty("configuration.console.parameters.title");

	Title title = new Title(caption, FontAwesome.COGS);

	addComponent(title);

    }

    /**
     * The style and the layout of the view.
     */
    private void setStyleAndLayout() {

	setMargin(new MarginInfo(false, true, false, true));

	setSpacing(true);
    }

}
