package org.torvis.console.view.configuration;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.torvis.console.util.PresentationUtil;
import org.torvis.service.services.InternatializationService;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;

/**
 * 
 * @author martin.letendre@release5.com
 *
 */
@Component
class DirectoryBindSection extends GridLayout {

    /** IDE. */
    private static final long serialVersionUID = -5042675189263216101L;

    private TextField nameTextField;

    private TextField hostnameTextField;

    private TextField portNumberTextField;

    private Button checkNetworkParameterButton;

    /**
     * Provide i18n services.
     */
    @Autowired
    private InternatializationService i18nService;

    DirectoryBindSection() {

	super(2, 4);

    }

    @PostConstruct
    void init() {

	setStyleAndLayout();

	addConnectionName();

	addHostname();

	addPortNumber();

	addCheckNetworkParameterButton();

    }

    /**
     * The style and the layout of the view.
     */
    private void setStyleAndLayout() {

	setSpacing(true);
    }

    private void addConnectionName() {

	final int ROW = 0;

	String caption = i18nService.getProperty("directory.configuration.name");

	nameTextField = new TextField(caption);

	nameTextField.setWidth(PresentationUtil.DEFAULT_WIDTH, Unit.PIXELS);

	addComponent(nameTextField, 0, ROW);

    }

    private void addHostname() {

	final int ROW = 1;

	String caption = i18nService.getProperty("directory.configuration.hostname");

	hostnameTextField = new TextField(caption);

	hostnameTextField.setWidth(PresentationUtil.DEFAULT_WIDTH, Unit.PIXELS);

	addComponent(hostnameTextField, 0, ROW);
    }

    private void addPortNumber() {

	final int ROW = 2;

	String caption = i18nService.getProperty("directory.configuration.port.number");

	portNumberTextField = new TextField(caption);

	portNumberTextField.setWidth(PresentationUtil.DEFAULT_WIDTH, Unit.PIXELS);

	String value = i18nService.getProperty("directory.ldap.default.port");

	portNumberTextField.setValue(value);

	addComponent(portNumberTextField, 0, ROW);
    }

    private void addCheckNetworkParameterButton() {

	final int ROW = 3;

	String caption = i18nService.getProperty("directory.configuration.check.network.parameters");

	checkNetworkParameterButton = new Button(caption);

	checkNetworkParameterButton.setIcon(FontAwesome.CHECK);

	checkNetworkParameterButton.setEnabled(false);

	checkNetworkParameterButton.setWidth(PresentationUtil.DEFAULT_WIDTH, Unit.PIXELS);

	addComponent(checkNetworkParameterButton, 0, ROW);

    }

}
