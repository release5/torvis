package org.torvis.console.view.configuration;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.torvis.component.basic.Title;
import org.torvis.console.util.PresentationUtil;
import org.torvis.service.model.configuration.NetworkProtocol;
import org.torvis.service.services.InternatializationService;

import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.VerticalLayout;

/**
 * Network connection parameters.
 * 
 * @author martin.letendre@release5.com
 */
@Component
class NetworkParameters extends VerticalLayout {

    /** IDE. */
    private static final long serialVersionUID = -2534873272665215226L;

    /**
     * Provide i18n services.
     */
    @Autowired
    private InternatializationService i18nService;

    @Autowired
    private DirectoryBindSection directoryBindSection;

    @PostConstruct
    void init() {

	setStyleAndLayout();

	addNetworkParamTitle();

	addConnectionTypeComboBox();

	addConnectionTypeSection();

    }
    
    

    /**
     * Title for the network parameters.
     */
    private void addNetworkParamTitle() {

	String caption = i18nService.getProperty("configuration.network.parameters.title");

	Title title = new Title(caption, FontAwesome.CLOUD);

	addComponent(title);

    }

    private void addConnectionTypeComboBox() {

	String caption = i18nService.getProperty("configuration.network.parameters.title");
	ComboBox typeComboBox = new ComboBox(caption);

	typeComboBox.setWidth(PresentationUtil.DEFAULT_WIDTH, Unit.PIXELS);

	typeComboBox.addItems(NetworkProtocol.values());

	String jdbcCaption = i18nService.getProperty("configuration.network.protocol.jdbc");
	typeComboBox.setItemCaption(NetworkProtocol.JDBC, jdbcCaption);

	String ldapCaption = i18nService.getProperty("configuration.network.protocol.ldap");
	typeComboBox.setItemCaption(NetworkProtocol.LDAP, ldapCaption);

	String ldapsCaption = i18nService.getProperty("configuration.network.protocol.ldaps");
	typeComboBox.setItemCaption(NetworkProtocol.LDAPS, ldapsCaption);

	typeComboBox.setValue(NetworkProtocol.LDAP);

	typeComboBox.setNullSelectionAllowed(false);

	addComponent(typeComboBox);

    }

    private void addConnectionTypeSection() {

	addComponent(directoryBindSection);

    }

    /**
     * The style and the layout of the view.
     */
    private void setStyleAndLayout() {

	setMargin(new MarginInfo(true, true, false, true));

	setSpacing(true);
    }

}
