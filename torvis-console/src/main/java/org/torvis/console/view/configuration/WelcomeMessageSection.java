package org.torvis.console.view.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.torvis.service.services.ConfigurationService;
import org.torvis.service.services.InternatializationService;
import org.torvis.service.services.util.FileSystemUtil;

import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

/**
 * Display a welcome message for the initial configuration view.
 * 
 * @author martin.letendre@release5.com
 *
 */
@Component
class WelcomeMessageSection extends GridLayout {

    /** IDE. */
    private static final long serialVersionUID = -8835724783093690323L;

    /**
     * Provide i18n services.
     */
    @Autowired
    private InternatializationService i18nService;

    private Map<String, String> tokens = new HashMap<String, String>();

    /**
     * 
     */
    @Autowired
    private ConfigurationService configurationService;

    WelcomeMessageSection() {

	super(2, 3);
    }

    /**
     * Display a welcome message for the initial configuration view.
     */
    @PostConstruct
    void init() {

	setStyleAndLayout();

	setTokens();

	addLogo();

	addWelcomeMessageLineOne();

	addSecureWarning();

    }

    private void setTokens() {

	tokens.put("user", FileSystemUtil.getUserName());

	tokens.put("torivs-path", FileSystemUtil.getTorvisHome());
    }

    private void addLogo() {

	ThemeResource themeResource = new ThemeResource("images/logo-small.png");

	Image logo = new Image(null, themeResource);

	logo.setDescription("The Torvis Logo");

	addComponent(logo, 0, 0, 0, 2);
    }

    /**
     * The style and the layout of the view.
     */
    private void setStyleAndLayout() {

	setMargin(new MarginInfo(true, false, true, true));

	setSpacing(true);

	this.setWidth(80, Unit.PERCENTAGE);

    }

    /**
     * Display a welcome message for the initial configuration view.
     */
    private void addWelcomeMessageLineOne() {

	String caption = i18nService.getProperty("welcome.message", tokens);

	Label welcomeMessageLabel = new Label();

	welcomeMessageLabel.setSizeUndefined();

	welcomeMessageLabel.setCaption(caption);

	welcomeMessageLabel.setCaptionAsHtml(true);

	addComponent(welcomeMessageLabel, 1, 1);

    }

    /**
     * Warn the users to secure ./torvis folder.
     */
    private void addSecureWarning() {

	String caption = "";

	caption = FontAwesome.WARNING.getHtml() + i18nService.getProperty("welcome.message.posix", tokens);

	Label secureWarningLabel = new Label();

	secureWarningLabel.setSizeUndefined();

	secureWarningLabel.setCaptionAsHtml(true);

	secureWarningLabel.setCaption(caption);

	addComponent(secureWarningLabel, 1, 2);

    }

}
