package org.torvis.console;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import com.vaadin.spring.server.SpringVaadinServlet;

/**
 * To initialize the Spring configuration and avoid having XML files.
 * 
 * @author martin.letendre@release5.com
 *
 */
public class WebContextInitializer implements WebApplicationInitializer {

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.web.WebApplicationInitializer#onStartup(javax.servlet
     * .ServletContext)
     */
    @Override
    public void onStartup(javax.servlet.ServletContext servletContext) throws ServletException {
	AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
	context.scan(SpringConfiguration.class.getPackage().getName());
	servletContext.addListener(new ContextLoaderListener(context));
	registerServlet(servletContext);
    }

    /**
     * Register the servlet configuration.
     * 
     * @param servletContext
     *            Defines a set of methods that a servlet uses to communicate
     *            with its servlet container, for example, to get the MIME type
     *            of a file, dispatch requests, or write to a log file.
     */
    private void registerServlet(ServletContext servletContext) {
	ServletRegistration.Dynamic dispatcher = servletContext.addServlet("vaadin", SpringVaadinServlet.class);
	dispatcher.setLoadOnStartup(1);
	dispatcher.addMapping("/*");
    }

}
