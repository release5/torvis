package org.torvis.console;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.spring.server.SpringVaadinServlet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

/**
 * The topmost component of the <strong>Torvis</strong> application.
 * 
 * @author martin.letendre@release5.com
 *
 */
@SpringUI
@Theme("torvis-theme")
public class ConsoleUserInterface extends UI {

    /** IDE. */
    private static final long serialVersionUID = 8717148934446616739L;

    /**
     * A Vaadin {@link ViewProvider} that fetches the views from the Spring
     * application context.
     */
    @Autowired
    private SpringViewProvider viewProvider;

    /**
     * The one and only Servlet of the of the <strong>Torvis</strong>
     * application.
     * 
     * @author martin.letendre@release5.com
     *
     */
    @WebServlet(value = "/*", asyncSupported = true, initParams = {
	    @WebInitParam(name = "contextClass", value = "org.springframework.web.context.support.AnnotationConfigWebApplicationContext"),
	    @WebInitParam(name = "contextConfigLocation", value = "org.torvis.console;.AppSpringConfiguration") })
    public static class Servlet extends SpringVaadinServlet {

	/** IDE. */
	private static final long serialVersionUID = -2886625470808739508L;
    }

    /**
     * Initialize the <strong>Torvis</strong> application.
     */
    @Override
    protected void init(VaadinRequest request) {
	final VerticalLayout root = new VerticalLayout();
	root.setSizeFull();
	setContent(root);

	Navigator navigator = new Navigator(this, root);
	navigator.addProvider(viewProvider);
    }

}
