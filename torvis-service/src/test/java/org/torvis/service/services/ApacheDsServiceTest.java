package org.torvis.service.services;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;
import org.torvis.service.model.exception.ApacheDirectoryException;
import org.torvis.service.services.util.FileSystemUtil;

/**
 * Unit tests for embedded Apache Directory Server.
 * 
 * @see org.torvis.service.services.ApacheDirectoryService
 * 
 * @author martin.letendre@release5.com
 *
 */
public class ApacheDsServiceTest {

    /**
     * Unit test for starting or stopping embedded Apache Directory Server.
     */
    @Test
    public void testApacheDsServiceStartStop() {

	try {

	    String path = FileSystemUtil.getUnitTestingApacheDsPath();

	    ApacheDirectoryService apacheDsService = new ApacheDirectoryService(path);

	    apacheDsService.start();

	    boolean hasInstanceDirectory = apacheDsService.hasInstanceDirectory();

	    Assert.assertTrue(hasInstanceDirectory);

	    apacheDsService.stop();

	    Assert.assertTrue(hasInstanceDirectory);

	    boolean success = apacheDsService.cleanInstanceDirectory();

	    Assert.assertTrue(success);

	    hasInstanceDirectory = apacheDsService.hasInstanceDirectory();

	    Assert.assertFalse(hasInstanceDirectory);

	} catch (ApacheDirectoryException exception) {

	    String message = exception.getMessage();

	    fail(message);

	}
    }
}
