package org.torvis.service.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.torvis.service.model.configuration.DirectoryBind;
import org.torvis.service.model.configuration.NetworkProtocol;
import org.torvis.service.model.configuration.authentication.BindAuthentification;
import org.torvis.service.model.directory.BindResult;
import org.torvis.service.model.directory.BindStatusCode;
import org.torvis.service.model.exception.ApacheDirectoryException;
import org.torvis.service.services.util.FileSystemUtil;

/**
 * 
 * @author martin.letendre@release5.com
 *
 */
public class DirectoryServicesTest {

    private DirectoryServices directoryServices;

    /**
     * Initialize the ConfigurationService.
     */
    @Before
    public void initialize() {

	directoryServices = new DirectoryServices();

    }

    /**
     * Test bind operation.
     */
    @Test
    public void testBind() {

	try {
	    DirectoryBind mockDirectoryBind = getMockDirectoryBind();

	    BindResult bind = directoryServices.bind(mockDirectoryBind);

	    assertEquals(BindStatusCode.FAILURE, bind.getStatusCode());

	    String path = FileSystemUtil.getUnitTestingApacheDsPath();

	    ApacheDirectoryService apacheDsService = new ApacheDirectoryService(path);

	    apacheDsService.start();

	    bind = directoryServices.bind(mockDirectoryBind);

	    assertEquals(BindStatusCode.SUCCESS, bind.getStatusCode());

	    apacheDsService.stop();

	    bind = directoryServices.bind(mockDirectoryBind);

	    assertEquals(BindStatusCode.FAILURE, bind.getStatusCode());

	} catch (ApacheDirectoryException e) {

	    fail("ApacheDirectoryException");

	}

    }

    /**
     * A mock ConsoleConfiguration object for testing purposes only.
     * 
     * @return A mock ConsoleConfiguration object for testing purposes only.
     */
    private DirectoryBind getMockDirectoryBind() {

	DirectoryBind directoryConnection = new DirectoryBind();

	directoryConnection.setHostname("localhost");

	directoryConnection.setName("test");

	directoryConnection.setPortNumber(10389);

	directoryConnection.setProtocol(NetworkProtocol.LDAP);

	BindAuthentification bindConfiguration = new BindAuthentification();

	bindConfiguration.setBindDn("uid=admin,ou=system");

	bindConfiguration.setBindPassword("secret");

	directoryConnection.setBindAuthentification(bindConfiguration);

	return directoryConnection;

    }

}
