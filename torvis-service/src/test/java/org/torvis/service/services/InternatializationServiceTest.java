package org.torvis.service.services;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Assert;
import org.junit.Test;

public class InternatializationServiceTest {

    @Test
    public void testInternatializationService() {

	try {
	    InternatializationService internatializationService = new InternatializationService();

	    String value1 = internatializationService.getProperty("test.1");

	    Assert.assertEquals("A", value1);

	    String value2 = internatializationService.getProperty("test.2");

	    Assert.assertEquals("B", value2);

	    String value3 = internatializationService.getProperty("test.3");
	    
	    Assert.assertNull(value3);

	    
	} catch (ConfigurationException e) {
	    Assert.fail();
	}
    }

}
