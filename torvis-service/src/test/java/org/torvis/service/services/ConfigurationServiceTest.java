package org.torvis.service.services;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.torvis.service.model.configuration.ConsoleConfiguration;
import org.torvis.service.model.configuration.DirectoryBind;
import org.torvis.service.model.configuration.NetworkProtocol;
import org.torvis.service.model.configuration.TorvisMaserUser;
import org.torvis.service.model.configuration.authentication.BindAuthentification;
import org.torvis.service.services.util.FileSystemUtil;

/**
 * Test the configuration service.
 * 
 * @see org.torvis.service.services.ConfigurationService
 * 
 * @author martin.letendre@release5.com
 *
 */
public class ConfigurationServiceTest {

    /**
     * Provide configuration services.
     */
    private ConfigurationService configurationService;

    /**
     * Initialize the ConfigurationService.
     */
    @Before
    public void initialize() {

	configurationService = new ConfigurationService();

    }

    /**
     * Test getConfigurationFile method.
     */
    @Test
    public void testGetTorvisHome() {

	String torvisHome = FileSystemUtil.getTorvisHome();

	Assert.assertNotNull(torvisHome);

    }

    /**
     * Test getConfigurationFile method.
     */
    @Test
    public void testGetConfigurationFile() {

	try {
	    File configurationFile = configurationService.getConfigurationFile();

	    Assert.assertNotNull(configurationFile);

	} catch (IOException exception) {

	    Assert.fail(exception.getMessage());

	}

    }

    /**
     * Test configurationFileExist method.
     */
    @Test
    public void testConfigurationFileExist() {

	try {

	    configurationService.createConfigurationFile();

	    boolean configurationFileExist = configurationService.configurationFileExist();

	    Assert.assertTrue(configurationFileExist);

	    configurationService.cleanConfiguration();

	    configurationFileExist = configurationService.configurationFileExist();

	    Assert.assertFalse(configurationFileExist);

	} catch (IOException exception) {

	    Assert.fail(exception.getMessage());
	}

    }

    @Test
    public void testCreateConfigurationDirectory() {

	try {

	    boolean success = configurationService.createConfigurationDirectory();

	    Assert.assertTrue(success);

	} catch (IOException e) {

	    Assert.fail(e.getMessage());
	}
    }

    /**
     * Test writeConsoleConfiguration method.
     */
    @Test
    public void testWriteConsoleConfiguration() {

	try {

	    ConsoleConfiguration mockConsoleConfiguration = getMockConsoleConfiguration();

	    configurationService.createConsoleConfiguration(mockConsoleConfiguration);

	    boolean configurationFileExist = configurationService.configurationFileExist();

	    Assert.assertTrue(configurationFileExist);

	    configurationService.cleanConfiguration();

	    configurationFileExist = configurationService.configurationFileExist();

	    Assert.assertFalse(configurationFileExist);

	} catch (IOException e) {

	    Assert.fail(e.getMessage());
	}
    }

    /**
     * Test getUserId method.
     */
    @Test
    public void testGetUserId() {

	String userName = FileSystemUtil.getUserName();

	Assert.assertNotNull(userName);
    }

    /**
     * Test javaToJson method.
     */
    @Test
    public void testJavaToJson() {

	ConsoleConfiguration mockConsoleConfiguration = getMockConsoleConfiguration();

	String javaToJson = configurationService.javaToJson(mockConsoleConfiguration);

	System.out.print(javaToJson);

	Assert.assertTrue(javaToJson.contains("directoryConnections"));

	Assert.assertTrue(javaToJson.contains("hostname"));

    }

    /**
     * Test the jsonToJava method.
     */
    @Test
    public void testJsonToJava() {

	ClassLoader classLoader = this.getClass().getClassLoader();

	URL resource = classLoader.getResource("console-configuration.json");

	File file = new File(resource.getPath());

	try {

	    String json = FileUtils.readFileToString(file);

	    ConsoleConfiguration consoleConfiguration = configurationService.jsonToJava(json);

	    TorvisMaserUser torvisMaserUser = consoleConfiguration.getTorvisMaserUser();

	    Assert.assertEquals("root", torvisMaserUser.getUsername());

	    Assert.assertEquals("qwerty", torvisMaserUser.getPassword());

	    List<DirectoryBind> directoryConnections = consoleConfiguration.getDirectoryConnections();

	    Assert.assertEquals(1, directoryConnections.size());

	    DirectoryBind directoryConnection = directoryConnections.get(0);

	    Assert.assertEquals("localhost", directoryConnection.getHostname());

	    Assert.assertEquals("test", directoryConnection.getName());

	    Assert.assertEquals(10389, directoryConnection.getPortNumber());

	    BindAuthentification bindConfiguration = directoryConnection.getBindAuthentification();

	    Assert.assertEquals("uid=admin,ou=system", bindConfiguration.getBindDn());

	    Assert.assertEquals("secret", bindConfiguration.getBindPassword());

	} catch (IOException exception) {

	    Assert.fail(exception.getMessage());

	}

    }

    /**
     * The readConsoleConfiguration and writeConsoleConfiguration methods.
     */
    @Test
    public void testUpdateConsoleConfiguration() {

	try {

	    ConsoleConfiguration mockConsoleConfiguration1 = getMockConsoleConfiguration();

	    configurationService.createConsoleConfiguration(mockConsoleConfiguration1);

	    assertConfiguration1();

	    ConsoleConfiguration mockConsoleConfiguration2 = getMockConsoleConfiguration2();

	    configurationService.createConsoleConfiguration(mockConsoleConfiguration2);

	    assertConfiguration2();

	} catch (IOException e) {

	    Assert.fail(e.getMessage());
	}
    }

    /**
     * Assert the configurations.
     * 
     * @throws IOException
     *             If a problem happens with IO.
     */
    private void assertConfiguration2() throws IOException {

	boolean configurationFileExist2 = configurationService.configurationFileExist();

	Assert.assertTrue(configurationFileExist2);

	Assert.assertTrue(configurationService.canReadWriteConfiguration());

	ConsoleConfiguration consoleConfiguration2 = configurationService.readConsoleConfiguration();

	TorvisMaserUser torvisMaserUser2 = consoleConfiguration2.getTorvisMaserUser();

	Assert.assertEquals("admin", torvisMaserUser2.getUsername());

	Assert.assertEquals("password", torvisMaserUser2.getPassword());

	List<DirectoryBind> directoryConnections2 = consoleConfiguration2.getDirectoryConnections();

	Assert.assertEquals(1, directoryConnections2.size());

	DirectoryBind directoryConnection2 = directoryConnections2.get(0);

	Assert.assertEquals("test2", directoryConnection2.getName());

	Assert.assertEquals(389, directoryConnection2.getPortNumber());

	Assert.assertEquals("127.0.0.1", directoryConnection2.getHostname());

	Assert.assertEquals(NetworkProtocol.LDAPS, directoryConnection2.getProtocol());
    }

    /**
     * Assert the configurations.
     * 
     * @throws IOException
     *             If a problem happens with IO.
     */
    private void assertConfiguration1() throws IOException {

	boolean configurationFileExist = configurationService.configurationFileExist();

	Assert.assertTrue(configurationFileExist);

	Assert.assertTrue(configurationService.canReadWriteConfiguration());

	ConsoleConfiguration consoleConfiguration = configurationService.readConsoleConfiguration();

	TorvisMaserUser torvisMaserUser = consoleConfiguration.getTorvisMaserUser();

	Assert.assertEquals("root", torvisMaserUser.getUsername());

	Assert.assertEquals("qwerty", torvisMaserUser.getPassword());

	List<DirectoryBind> directoryConnections = consoleConfiguration.getDirectoryConnections();

	Assert.assertEquals(1, directoryConnections.size());

	DirectoryBind directoryConnection = directoryConnections.get(0);

	Assert.assertEquals("test", directoryConnection.getName());

	Assert.assertEquals(10389, directoryConnection.getPortNumber());

	Assert.assertEquals("localhost", directoryConnection.getHostname());

	Assert.assertEquals(NetworkProtocol.LDAP, directoryConnection.getProtocol());
    }

    /**
     * A mock ConsoleConfiguration object for testing purposes only.
     * 
     * @return A mock ConsoleConfiguration object for testing purposes only.
     */
    private ConsoleConfiguration getMockConsoleConfiguration() {

	ConsoleConfiguration consoleConfiguration = new ConsoleConfiguration();

	TorvisMaserUser torvisMaserUser = new TorvisMaserUser();

	torvisMaserUser.setUsername("root");

	torvisMaserUser.setPassword("qwerty");

	consoleConfiguration.setTorvisMaserUser(torvisMaserUser);

	DirectoryBind directoryConnection = new DirectoryBind();

	directoryConnection.setHostname("localhost");

	directoryConnection.setName("test");

	directoryConnection.setPortNumber(10389);

	directoryConnection.setProtocol(NetworkProtocol.LDAP);

	BindAuthentification bindConfiguration = new BindAuthentification();

	bindConfiguration.setBindDn("uid=admin,ou=system");

	bindConfiguration.setBindPassword("secret");

	directoryConnection.setBindAuthentification(bindConfiguration);

	consoleConfiguration.addDirectoryConnection(directoryConnection);

	return consoleConfiguration;

    }

    /**
     * A mock ConsoleConfiguration object for testing purposes only.
     * 
     * @return A mock ConsoleConfiguration object for testing purposes only.
     */
    private ConsoleConfiguration getMockConsoleConfiguration2() {

	ConsoleConfiguration consoleConfiguration = new ConsoleConfiguration();

	TorvisMaserUser torvisMaserUser = new TorvisMaserUser();

	torvisMaserUser.setUsername("admin");

	torvisMaserUser.setPassword("password");

	consoleConfiguration.setTorvisMaserUser(torvisMaserUser);

	DirectoryBind directoryConnection = new DirectoryBind();

	directoryConnection.setHostname("127.0.0.1");

	directoryConnection.setName("test2");

	directoryConnection.setPortNumber(389);

	directoryConnection.setProtocol(NetworkProtocol.LDAPS);

	BindAuthentification bindConfiguration = new BindAuthentification();

	bindConfiguration.setBindDn("uid=root,ou=system");

	bindConfiguration.setBindPassword("service");

	directoryConnection.setBindAuthentification(bindConfiguration);

	consoleConfiguration.addDirectoryConnection(directoryConnection);

	return consoleConfiguration;

    }

}
