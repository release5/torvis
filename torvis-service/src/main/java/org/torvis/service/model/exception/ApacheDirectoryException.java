package org.torvis.service.model.exception;

/**
 * Exception for Embedded Apache Directory Server (used for unit testing).
 * 
 * @author martin.letendre@release5.com
 *
 */
public class ApacheDirectoryException extends Exception {

    /** IDE. */
    private static final long serialVersionUID = 3832509742524519814L;

    /**
     * Exception for Embedded Apache Directory used for unit testing.
     * 
     * @param message
     *            Exception message to display.
     */
    public ApacheDirectoryException(String message) {

	super(message);
    }

}
