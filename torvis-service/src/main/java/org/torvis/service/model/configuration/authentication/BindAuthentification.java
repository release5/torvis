package org.torvis.service.model.configuration.authentication;

/**
 * The configuration (username and password) to bind (Create connection [opening
 * a socket between the client and the server] to a Directory [LDAP] server).
 * 
 * @author martin.letendre@release5.com
 *
 */
public class BindAuthentification {

    /**
     * Username to connect to Directory (LDAP) server.<br/>
     * Example: "uid=admin,ou=system".
     */
    private String bindDn;

    /**
     * Password to connect to Directory (LDAP) server. <br/>
     * Example: "secret".
     */

    private String bindPassword;

    /**
     * Username to connect to Directory (LDAP) server.
     * 
     * @return Example: "uid=admin,ou=system".
     */
    public String getBindDn() {
	return bindDn;
    }

    /**
     * Username to connect to Directory (LDAP) server.
     * 
     * @param bindDn
     *            Example: "uid=admin,ou=system".
     */
    public void setBindDn(String bindDn) {
	this.bindDn = bindDn;
    }

    /**
     * Password to connect to Directory (LDAP) server.
     * 
     * @return Example: "secret".
     */
    public String getBindPassword() {
	return bindPassword;
    }

    /**
     * Password to connect to Directory (LDAP) server.
     * 
     * @param bindPassword
     *            Example: "secret".
     */
    public void setBindPassword(String bindPassword) {
	this.bindPassword = bindPassword;
    }

}
