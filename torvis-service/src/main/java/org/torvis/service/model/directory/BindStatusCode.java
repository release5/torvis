package org.torvis.service.model.directory;

/**
 * Status code for the bind operation.
 * 
 * @author martin.letendre@release5.com
 *
 */
public enum BindStatusCode {
	/**
	 * The bind operation was a success.
	 */
    SUCCESS,

	/**
	 * The bind operation is a failure.
	 */
    FAILURE;

}
