package org.torvis.service.model.configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * The configuration of the torvis console. This configuration is serialized in
 * <strong>Json</strong> on disk.
 * 
 * @author martin.letendre@release5.com
 *
 */
public class ConsoleConfiguration {

    /**
     * The master username/password for Torvis console.
     */
    private TorvisMaserUser torvisMaserUser;

    /**
     * A list of directory (LDAP) connections.
     */
    private List<DirectoryBind> directoryConnections = new ArrayList<DirectoryBind>();

    /**
     * Add a directory (LDAP) connection to the configuration.
     * 
     * @param directoryConnection
     *            A directory (LDAP) connection.
     * @return The operation is a success ?.
     */
    public boolean addDirectoryConnection(DirectoryBind directoryConnection) {

	return directoryConnections.add(directoryConnection);

    }

    /**
     * The master username/password for Torvis console.
     * 
     * @return The master username/password for Torvis console.
     */
    public TorvisMaserUser getTorvisMaserUser() {
	return torvisMaserUser;
    }

    /**
     * The master username/password for Torvis console.
     * 
     * @param torvisMaserUser
     *            The master username/password for Torvis console.
     */
    public void setTorvisMaserUser(TorvisMaserUser torvisMaserUser) {
	this.torvisMaserUser = torvisMaserUser;
    }

    /**
     * A copy of the list of directory (LDAP) connections.
     * 
     * @return A copy of the list of directory (LDAP) connections.
     */
    public List<DirectoryBind> getDirectoryConnections() {

	return new ArrayList<DirectoryBind>(directoryConnections);
    }

}
