package org.torvis.service.model.configuration;

/**
 * The master username/password for Torvis console.
 * 
 * @author martin.letendre@release5.com
 *
 */
public class TorvisMaserUser {

	/**
	 * The master username for Torvis console. Example: "admin".
	 */
	private String username;

	/**
	 * The master password for Torvis console. Example: "secret".
	 */
	private String password;

	/**
	 * The master username for Torvis console.
	 * 
	 * @return Example: "admin".
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * The master username for Torvis console.
	 * 
	 * @param username
	 *            Example: "admin".
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * The master password for Torvis console.
	 * 
	 * @return Example: "secret".
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * The master password for Torvis console.
	 * 
	 * @param password
	 *            Example: "secret".
	 */

	public void setPassword(String password) {
		this.password = password;
	}

}
