package org.torvis.service.model.configuration;

import java.util.Hashtable;

import javax.naming.Context;

import org.torvis.service.model.configuration.authentication.BindAuthentification;

/**
 * Represent a configuration to a single Directory (LDAP) server.
 * 
 * @author martin.letendre@release5.com
 *
 */
public class DirectoryBind {

    /**
     * The name of the connection (an id).
     */
    private String name;

    /**
     * The protocol used for binding to the Directory (LDAP) Server.
     * 
     * @see org.torvis.service.model.configuration.NetworkProtocol
     */
    private NetworkProtocol protocol;

    /**
     * The hostname or ip address to bind to the Directory (LDAP) Server.<br/>
     * Example: "localhost" or "127.0.0.1".
     */
    private String hostname;

    /**
     * The port number to bind to the Directory (LDAP) Server.<br/>
     * Example: 389 or 636
     * 
     */
    private int portNumber;

    /**
     * The configuration to bind to a Directory [LDAP] server).
     */
    private BindAuthentification bindAuthentification;

    /**
     * The protocol used for binding to the Directory (LDAP) Server.
     * 
     * @return Example: LdapProtocol.LDAP or LdapProtocol.LDAPS.
     */
    public NetworkProtocol getProtocol() {
	return protocol;
    }

    /**
     * The protocol used for binding to the Directory (LDAP) Server.
     * 
     * @param protocol
     *            Example: LdapProtocol.LDAP or LdapProtocol.LDAPS.
     */
    public void setProtocol(NetworkProtocol protocol) {
	this.protocol = protocol;
    }

    /**
     * The name of the connection (an id).
     * 
     * @return The name of the connection (an id).
     */
    public String getName() {
	return name;
    }

    /**
     * The name of the connection (an id).
     * 
     * @param name
     *            The name of the connection (an id). Example: "qa-connection".
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * The port number to bind to the Directory (LDAP) Server.
     *
     * 
     * @return Example: 389 or 636.
     */
    public int getPortNumber() {
	return portNumber;
    }

    /**
     * The port number to bind to the Directory (LDAP) Server.
     * 
     * @param portNumber
     *            Example: 389 or 636.
     */
    public void setPortNumber(int portNumber) {
	this.portNumber = portNumber;
    }

    /**
     * The hostname or ip address to bind to the Directory (LDAP) Server.
     * 
     * @return Example: "localhost" or "127.0.0.1".
     */
    public String getHostname() {
	return hostname;
    }

    /**
     * The hostname or ip address to bind to the Directory (LDAP) Server.
     * 
     * @param hostname
     *            Example: "localhost" or "127.0.0.1".
     */
    public void setHostname(String hostname) {
	this.hostname = hostname;
    }

    /**
     * The configuration (username and password) to bind (Create connection
     * [opening a socket between the client and the server] to a Directory
     * [LDAP] server).
     * 
     * @return The configuration to bind to a Directory [LDAP] server).
     */
    public BindAuthentification getBindAuthentification() {
	return bindAuthentification;
    }

    /**
     * The provider url.
     * 
     * @return Example: "ldap://localhost:389".
     */
    private String getProviderUrl() {

	String url = getProtocol().getUrlPrefix() + getHostname() + ":" + getPortNumber();

	return url;
    }

    /**
     * The authentification (username and password) to bind (Create connection
     * [opening a socket between the client and the server] to a Directory
     * [LDAP] server).
     * 
     * @param bindConfiguration
     *            The configuration to bind to a Directory [LDAP] server).
     */
    public void setBindAuthentification(BindAuthentification bindConfiguration) {
	this.bindAuthentification = bindConfiguration;
    }

    /**
     * Environment used to create the initial DirContext.
     * 
     * @return Environment used to create the initial DirContext.
     */
    public Hashtable<String, String> getDirContextEnvironment() {

	Hashtable<String, String> env = new Hashtable<String, String>();

	env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
	env.put(Context.PROVIDER_URL, getProviderUrl());
	env.put(Context.SECURITY_AUTHENTICATION, "simple");

	BindAuthentification athentification = getBindAuthentification();
	env.put(Context.SECURITY_PRINCIPAL, athentification.getBindDn());
	env.put(Context.SECURITY_CREDENTIALS, athentification.getBindPassword());

	return env;
    }

}
