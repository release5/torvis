package org.torvis.service.model.directory;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;

/**
 * Result of the bind operation (success or failure).
 * 
 * @author martin.letendre@release5.com
 *
 */
public class BindResult {

    /**
     * Status code for the bind operation.
     */
    private BindStatusCode statusCode;

    /**
     * The directory service interface, containing methods for examining and
     * updating attributes associated with objects, and for searching the
     * directory.
     */
    private DirContext directoryContext;

    /**
     * * This is the superclass of all exceptions thrown by operations in the
     * Context and DirContext interfaces.
     */
    private NamingException namingException;

    /**
     * Status code for the bind operation.
     * 
     * @return Example BindStatusCode.SUCCESS
     */
    public BindStatusCode getStatusCode() {

	return statusCode;

    }

    /**
     * The directory service interface, containing methods for examining and
     * updating attributes associated with objects, and for searching the
     * directory.
     * 
     * @return The directory service interface, containing methods for examining
     *         and updating attributes associated with objects, and for
     *         searching the directory.
     */
    public DirContext getDirectoryContext() {
	return directoryContext;
    }

    /**
     * The directory service interface, containing methods for examining and
     * updating attributes associated with objects, and for searching the
     * directory.
     * 
     * @param directoryContext
     *            The directory service interface, containing methods for
     *            examining and updating attributes associated with objects, and
     *            for searching the directory.
     */
    public void setDirectoryContext(DirContext directoryContext) {

	this.directoryContext = directoryContext;

	statusCode = BindStatusCode.SUCCESS;
    }

    /**
     * Exception thrown by operations in the Context and DirContext interfaces.
     * 
     * @return Exception thrown by operations in the Context and DirContext
     *         interfaces.
     */
    public NamingException getNamingException() {

	return namingException;

    }

    /**
     * Exception thrown by operations in the Context and DirContext interfaces.
     * 
     * @param namingException
     *            Exception thrown by operations in the Context and DirContext
     *            interfaces.
     */
    public void setNamingException(NamingException namingException) {

	this.namingException = namingException;

	statusCode = BindStatusCode.FAILURE;

    }

}
