package org.torvis.service.model.configuration;

/**
 * Possible LDAP protocols to bind to the directory server.
 * 
 * @author martin.letendre@release5.com
 *
 */
public enum NetworkProtocol {

	/**
	 * Lightweight Directory Application Protocol.
	 */
    LDAP("ldap://"),

	/**
	 * Lightweight Directory Application Protocol Secure.
	 */
    LDAPS("ldaps://"),

	/**
	 * Java Database Connectivity.
	 */
    JDBC("jdbc:");

    private String urlPrefix;

    NetworkProtocol(String urlPrefix) {

	this.urlPrefix = urlPrefix;
    }

    public String getUrlPrefix() {
	return urlPrefix;
    }

}
