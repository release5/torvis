package org.torvis.service.services;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.text.StrSubstitutor;
import org.springframework.stereotype.Component;

/**
 * Provide i18n services.
 * 
 * @author martin.letendre@release5.com
 *
 */
@Component
public class InternatializationService {

    /**
     * This is the "classic" Properties loader which loads the values from a
     * single or multiple files (which can be chained with "include =". All
     * given path references are either absolute or relative to the file name
     * supplied in the constructor.
     */
    private PropertiesConfiguration config = new PropertiesConfiguration();

    /**
     * Provide i18n services.
     * 
     * @throws ConfigurationException
     *             Any exception that occurs while initializing a Configuration
     *             object.
     */
    public InternatializationService() throws ConfigurationException {

	File file = new File("labels.properties");

	config.setFile(file);

	config.load();
    }

    /**
     * The property value with this key.
     * 
     * @param key
     *            The unique id for this property.
     * @return The property value.
     */
    public String getProperty(String key) {

	return getProperty(key, null);

    }

    /**
     * The property value with this key.
     * 
     * @param key
     *            The unique id for this property.
     * @return The property value.
     */

    public String getProperty(String key, Map<String, String> tokens) {

	Object property = config.getProperty(key);

	if (property instanceof String) {

	    String expression = (String) config.getProperty(key);

	    return replaceTokens(expression, tokens);

	} else if (property instanceof List) {

	    @SuppressWarnings("unchecked")
	    List<String> content = (List<String>) property;

	    if (content.size() > 0) {

		String expression = content.get(0);

		return replaceTokens(expression, tokens);

	    }

	}

	return null;
    }

    /**
     * Replace tokens in an expression.
     * 
     * @param expression
     *            The expression with tokens to replace.
     * @param tokens
     *            A map containing the tokens.
     * @return The result of the replacement.
     */
    private String replaceTokens(String expression, Map<String, String> tokens) {

	if (tokens == null || tokens.size() < 0) {

	    return expression;
	}

	StrSubstitutor sub = new StrSubstitutor(tokens);

	String result = sub.replace(expression);

	return result;

    }

}
