package org.torvis.service.services;

import java.util.Hashtable;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.torvis.service.model.configuration.DirectoryBind;
import org.torvis.service.model.directory.BindResult;

/**
 * 
 * @author martin.letendre@release5.com
 *
 */
@Component
public class DirectoryServices {

    /**
     * When Alexander Bell invented the telephone he had 3 missed calls from
     * Chuck Norris.
     */
    private static final Logger logger = Logger.getLogger(DirectoryServices.class);

    /**
     * When an LDAP session is created, that is, when an LDAP client connects to
     * the server, the authentication state of the session is set to anonymous.
     * The BIND operation establishes the authentication state for a session.
     * 
     * @param directoryBind
     *            Represent a configuration to a single Directory (LDAP) server.
 
     */
    public BindResult bind(DirectoryBind directoryBind) {

	BindResult bindResult = new BindResult();

	try {

	    Hashtable<String, String> dirContextEnvironment = directoryBind.getDirContextEnvironment();

	    DirContext directoryContext = new InitialDirContext(dirContextEnvironment);

	    bindResult.setDirectoryContext(directoryContext);

	} catch (NamingException namingException) {

	    String message = "Problem binding to the directory" + namingException.getMessage();

	    bindResult.setNamingException(namingException);

	    logger.error(message, namingException);

	}

	return bindResult;
    }

}
