package org.torvis.service.services;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.directory.server.ApacheDsService;
import org.apache.directory.server.core.api.InstanceLayout;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.torvis.service.model.exception.ApacheDirectoryException;
import org.torvis.service.services.util.FileSystemUtil;

/**
 * Service to stop,start,configure an embedded Apache Directory Server.
 * 
 * @author martin.letendre@release5.com
 *
 */
@Component
public class ApacheDirectoryService {

    /**
     * A class used to start various servers in a given {@link InstanceLayout}.
     * 
     * @see org.apache.directory.server.ApacheDsService
     */
    private ApacheDsService service;

    /**
     * If you spell Chuck Norris in Scrabble, you win, forever.
     */
    private static final Logger logger = Logger.getLogger(ApacheDirectoryService.class);

    /**
     * Apache DS configuration for the instance.
     */
    private InstanceLayout instanceLayout;

    /**
     * To start the Apache DS from java/eclipse.
     * 
     * @param args
     *            No args.
     * @throws Exception
     *             If problem happens.
     */
    public static void main(String[] args) throws Exception {

	String unitTestingApacheDsPath = FileSystemUtil.getUnitTestingApacheDsPath();

	ApacheDirectoryService apacheDsService = new ApacheDirectoryService(unitTestingApacheDsPath);

	apacheDsService.start();
    }

    /**
     * Service to stop,start,configure an embedded Apache Directory Server.
     * 
     * @param instanceDirectoryPath
     *            The path for embedded Apache Directory Server
     *            configuration/data files.
     */
    public ApacheDirectoryService(String instanceDirectoryPath) {

	service = new ApacheDsService();

	this.instanceLayout = new InstanceLayout(instanceDirectoryPath);

    }

    /**
     * Initializing a Apache DS instance.
     * 
     * @throws ApacheDirectoryException
     *             Exception when starting Embedded Apache Directory Server.
     */
    public void start() throws ApacheDirectoryException {

	try {

	    service.start(instanceLayout);

	} catch (Exception exception) {

	    final String message = "Failed to start the apache directory server.";

	    logger.error(message, exception);

	    throw new ApacheDirectoryException(message);

	}
    }

    /**
     * Stop a Apache DS instance.
     * 
     * @throws ApacheDirectoryException
     *             Exception when starting Embedded Apache Directory Server.
     */
    public void stop() throws ApacheDirectoryException {

	if (service != null) {

	    try {

		service.stop();

	    } catch (Exception exception) {

		final String message = "Failed to stop the apache directory server.";

		logger.error(message, exception);

		throw new ApacheDirectoryException(message);
	    }
	}
    }

    /**
     * Does the Embedded Apache Directory Server has a configuration ?.
     * 
     * @return Does the Embedded Apache Directory Server has a configuration ?.
     */
    public boolean hasInstanceDirectory() {

	File instanceDirectory = instanceLayout.getInstanceDirectory();

	return instanceDirectory.exists();
    }

    /**
     * Delete the Embedded Apache Directory Server configuration.
     * 
     * @return Does the operation is a success?.
     */
    public boolean cleanInstanceDirectory() {

	File instanceDirectory = instanceLayout.getInstanceDirectory();

	try {
	    FileUtils.deleteDirectory(instanceDirectory);

	    return true;

	} catch (IOException e) {

	    return false;
	}

    }

}
