package org.torvis.service.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;
import org.torvis.service.model.configuration.ConsoleConfiguration;
import org.torvis.service.services.util.FileSystemUtil;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Provide configuration services.
 * <ol>
 * <li>Check if the configuration exist under the current user profile.</li>
 * <li>Provide an existing configuration bean (ConsoleConfiguration)</li>
 * <li>Save or update a valid configuration file
 * (./tovis/console-configuration.json).</li>
 * <li>Clean the torvis directory.</li>
 * </ol>
 * 
 * @author martin.letendre@release5.com
 *
 */
@Component
public class ConfigurationService {

    /**
     * The <strong>Torvis</strong> configuration directory (the file).
     * 
     * @return The <strong>Torvis</strong> configuration directory (the file).
     */
    public File getConfigurationDirectory() {

	String configurationPath = FileSystemUtil.getTorvisHome();

	File configurationDirectory = new File(configurationPath);

	return configurationDirectory;

    }

    /**
     * The <strong>Torvis</strong> configuration file path.
     * 
     * @return Example: "/home/release5/.torvis/console-configuration.json"
     * @throws IOException
     *             If a problem happens with IO.
     */
    public File getConfigurationFile() throws IOException {

	String configurationPath = FileSystemUtil.getConfigurationPath();

	File configurationFile = new File(configurationPath);

	return configurationFile;

    }

    /**
     * Create the configuration directory.
     * 
     * @return Does the operation is a sucess ?.
     * @throws IOException
     *             If a problem happens with IO.
     */
    public boolean createConfigurationDirectory() throws IOException {

	File configurationDirectory = getConfigurationDirectory();

	boolean success = configurationDirectory.mkdir();

	setPermissions(configurationDirectory);

	return success;
    }

    /**
     * Set POSIX permission RWX --- --- on the file.
     * 
     * @param configurationDirectory
     *            Directory that contains the configuration files. Example
     *            /home/admin/.torvis.
     * @throws IOException
     *             If a problem happens with IO.
     */
    private void setPermissions(File configurationDirectory) throws IOException {

	if (isPosix()) {

	    setPosixPermissions(configurationDirectory);

	} else {

	    setGenericPermissions(configurationDirectory);

	}
    }

    /**
     * Are we running on a POSIX compatible system ?.
     * 
     * @return Are we running on a POSIX compatible system ?.
     */
    public static boolean isPosix() {

	final boolean isPosix = FileSystems.getDefault().supportedFileAttributeViews().contains("posix");

	return isPosix;

    }

    /**
     * Are we running on a POSIX compatible system ?.
     * 
     * @param configurationDirectory
     *            Directory that contains the configuration files. Example
     *            /home/admin/.torvis.
     * @throws IOException
     *             If a problem happens with IO.
     */
    private void setGenericPermissions(File configurationDirectory) throws IOException {

	configurationDirectory.setReadable(false, true);

	configurationDirectory.setWritable(false, true);

	configurationDirectory.setExecutable(false);

    }

    /**
     * Set POSIX permission RWX --- --- on the file.
     * 
     * @param configurationDirectory
     *            Directory that contains the configuration files. Example
     *            /home/admin/.torvis.
     * @throws IOException
     *             If a problem happens with IO.
     */
    private void setPosixPermissions(File configurationDirectory) throws IOException {

	Set<PosixFilePermission> perms = new HashSet<PosixFilePermission>();

	perms.add(PosixFilePermission.OWNER_READ);
	perms.add(PosixFilePermission.OWNER_WRITE);
	perms.add(PosixFilePermission.OWNER_EXECUTE);

	String directoryPath = configurationDirectory.getPath();

	Files.setPosixFilePermissions(Paths.get(directoryPath), perms);
    }

    /**
     * Create the <strong>Torvis Console</strong> configuration file.
     * 
     * @throws IOException
     *             If a problem happens with IO.
     */
    public void createConfigurationFile() throws IOException {

	File configurationFile = getConfigurationFile();

	FileUtils.touch(configurationFile);
    }

    /**
     * Does the configuration file exist ?.
     * 
     * @return Example: Does the configuration file under
     *         "/home/release5/.torvis/console-configuration.json" exist ?.
     * @throws IOException
     *             If a problem happens with IO.
     */
    public boolean configurationFileExist() throws IOException {

	File configurationFile = getConfigurationFile();

	return configurationFileExist(configurationFile);
    }

    /**
     * Does the configuration file exist ?.
     * 
     * @return The file to check.
     */
    private boolean configurationFileExist(File configurationFile) {

	return configurationFile.exists() && !configurationFile.isDirectory();
    }

    /**
     * Does the configuration file is readable and writable ?.
     * 
     * @return Example: Does the configuration file under
     *         "/home/release5/.torvis/console-configuration.json" is readable
     *         and writable ?.
     * @throws IOException
     *             If a problem happens with IO.
     */
    public boolean canReadWriteConfiguration() throws IOException {

	File configurationFile = getConfigurationFile();

	return configurationFile.canRead() && configurationFile.canWrite();
    }

    /**
     * Write console configuration to disk.
     * 
     * @param consoleConfiguration
     *            The configuration of the torvis console.
     * @throws IOException
     *             If a problem happens with IO.
     */
    public void createConsoleConfiguration(ConsoleConfiguration consoleConfiguration) throws IOException {

	createConfigurationDirectory();

	String json = javaToJson(consoleConfiguration);

	writeConsoleConfigurationFile(json);

    }

    /**
     * Write the hson configuration to disk.
     * 
     * @param json
     *            A json file.
     * @throws IOException
     *             If a problem happens with IO.
     */
    private void writeConsoleConfigurationFile(String json) throws IOException {

	File configurationFile = getConfigurationFile();

	FileUtils.write(configurationFile, json);

	setPermissions(configurationFile);

    }

    /**
     * Read the json configuration file from disk (under
     * "$HOME/.torvis/console-configuration.json") and convert it to a Java
     * Bean.
     * 
     * @return The configuration of the torvis console.
     * @throws IOException
     *             If a problem happens with IO.
     */
    public ConsoleConfiguration readConsoleConfiguration() throws IOException {

	File configurationFile = getConfigurationFile();

	String json = FileUtils.readFileToString(configurationFile);

	ConsoleConfiguration consoleConfiguration = jsonToJava(json);

	return consoleConfiguration;

    }

    /**
     * Delete the configuration directory "$HOME/.torvis/"
     * 
     * @throws IOException
     *             If a problem happens with IO.
     */
    public void cleanConfiguration() throws IOException {

	String torvisHome = FileSystemUtil.getTorvisHome();

	File directory = new File(torvisHome);

	FileUtils.deleteDirectory(directory);
    }

    /**
     * Serialize a Java object into JSON.
     * 
     * @param object
     *            A java bean.
     * @return A String in JSON format.
     */
    String javaToJson(Object object) {

	Gson gson = new GsonBuilder().setPrettyPrinting().create();

	return gson.toJson(object);

    }

    /**
     * Convert a JSON String into a Console Configuration File.
     * 
     * @param json
     *            A JSON expression.
     * @return The configuration of the torvis console.
     */
    ConsoleConfiguration jsonToJava(String json) {

	Gson gson = new GsonBuilder().create();

	ConsoleConfiguration consoleConfiguration = gson.fromJson(json, ConsoleConfiguration.class);

	return consoleConfiguration;

    }

}
