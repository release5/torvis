package org.torvis.service.services.util;

import java.util.Properties;

public class FileSystemUtil {

    /**
     * User home properties name.
     */
    private static final String USER_HOME_KEY = "user.home";

    /**
     * The key for user.dir property.
     */
    private static final String USER_DIR_KEY = "user.dir";

    /**
     * The key for user.name property.
     */
    private static final String USER_NAME_KEY = "user.name";

    private static final String APACHE_DS_DIR = "/apacheDs";

    /**
     * The <strong>Torvis</strong> configuration directory.
     */
    private static final String TORVIS_HOME = "/.torvis/";

    /**
     * The console configuration file.
     */
    private static final String CONSOLE_CONFIGURATION_FILE_PATH = "console-configuration.json";

    /**
     * User home path.
     * 
     * @return Example: "/home/release5"
     */
    private static String getUserHome() {

	Properties properties = System.getProperties();

	String userHome = (String) properties.get(USER_HOME_KEY);

	return userHome;

    }

    /**
     * Directory where java was run from, where you started the JVM. Does not
     * have to be within the user's home directory. It can be anywhere where the
     * user has permission to run java.
     * 
     * @return Example "/home/release5".
     */
    public static String getUserDir() {

	Properties properties = System.getProperties();

	String userHome = (String) properties.get(USER_DIR_KEY);

	return userHome;
    }

    /**
     * The username of the user running Torvis.
     * 
     * @return Example: "release5"
     */
    public static String getUserName() {

	Properties properties = System.getProperties();

	String userHome = (String) properties.get(USER_NAME_KEY);

	return userHome;

    }

    /**
     * The <strong>Torvis</strong> configuration path.
     * 
     * @return Example: "/home/release5/.torvis/"
     */
    public static String getTorvisHome() {

	final String userHome = getUserHome();

	return userHome + TORVIS_HOME;
    }

    /**
     * Return a path for Aapache directory configuration for unit testing.
     * 
     * @return Example
     *         "/home/release5/git/torvis/torvis-service/target/apacheDs"
     */
    public static String getUnitTestingApacheDsPath() {

	String path = FileSystemUtil.getUserDir();

	return path + "/target" + APACHE_DS_DIR;

    }

    /**
     * Return a path for Apache directory configuration for embedded execution.
     * 
     * @return Example "/home/release5/.torvis/apacheDs"
     */
    public static String getEmbeddedApacheDsPath() {

	String path = getTorvisHome();

	return path + APACHE_DS_DIR;

    }

    /**
     * The path for Torvis console configuration.
     * 
     * @return Example "/home/release5/.torvis/console-configuration.json".
     */
    public static String getConfigurationPath() {

	return getTorvisHome() + CONSOLE_CONFIGURATION_FILE_PATH;
    }

}
